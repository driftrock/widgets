$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "driftrock_widgets/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "driftrock_widgets"
  s.version     = DriftrockWidgets::VERSION
  s.authors     = ["Driftrock"]
  s.email       = ["accounts@driftrock.com"]
  s.homepage    = "https://github.com/forward-labs/driftrock_widgets"
  s.summary     = "Widgets scripts for driftrock apps"
  s.description = "Widgets classes, helpers, javascripts and styles"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]

  s.add_dependency "rails", "~> 4.0.0"
  s.add_dependency "driftrock_channels", ">= 0.2.0"
  s.add_dependency "driftrock_reports", ">= 0.4.1"

  s.add_development_dependency "rspec-rails"
  s.add_development_dependency "sqlite3"
  s.add_development_dependency "jasmine", "~> 1.3.2"
  s.add_development_dependency "pry"
end
