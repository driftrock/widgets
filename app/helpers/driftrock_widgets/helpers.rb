module DriftrockWidgets
  module Helpers

    def widget(opts)
      DriftrockWidgets::Widget.new(self, channels_for_company, request, opts).widget_element
    end

    def render_widget_graph(element_id)
      render 'driftrock_widgets/graph', element_id: element_id
    end

  end
end
