;var Graph = Graph || {};

Graph.data = Graph.data || {};
Graph.data.operators = {
  //OPERATORS
  identity : function(val){
    return val;
  },
  ratio : function(numerator, denominator){
    return denominator > 0 ? numerator/denominator : 0;
  },
  percentage : function(numerator, denominator){
    return Graph.data.operators.ratio(numerator, denominator) * 100.0;
  },
  getLast : function(val){
    return Graph.data.operators.identity(val);
  },
  ratioPerThousand : function(numerator, denominator){
    return Graph.data.operators.ratio(numerator, denominator) * 1000.0;
  },
  callOperator : function(name, argsArray){
    return Graph.data.operators[name].apply(this, argsArray);
  }
};

Graph.functions = Graph.functions || {};
Graph.functions.total = function(){
  return function(){
    var numerator, denominator, last, lastValue;
    var graph = this;
    var reportType = graph.reportType,
        data = graph.parsedData,
        hasGrouping = Graph.utils.exists(graph.groupBy);

    if(reportType.type == "get-last"){
      last = data[data.length - 1];
      lastValue = 0;
      if(typeof(last) !== 'undefined') {
        lastValue = last[reportType.values[0]];
      }
      return lastValue;
    }else{
      var operatorName = Graph.utils.dashedNameToCamelized(reportType.type);
      var operatorParams = [];

      var sum = function(valueType){
        return data.reduce(function(sum, row){
          if(hasGrouping){
            var groupKeys = graph.allGroupByValues;
            return sum += groupKeys.reduce(function(groupSum, groupKey){
              return groupSum += parseFloat(row.variables[valueType][groupKey] || 0);
            }, 0);
          }else{
            return sum += parseFloat(row.variables[valueType] || 0);
          }
        }, 0);
      };

      //get variable values
      reportType.values.forEach(function(valueType){
        var val = sum(valueType);
        operatorParams.push(val);
      });

      return Graph.data.operators.callOperator(operatorName, operatorParams);
    }
  };
}();

//Works out the actual graph values from the transformed data
Graph.functions.parseData = function(){
  var addRowValues = function(){
    var service = this, keys = service.keys, valueObject;
    var graph = service.graph;
    var firstReport = service.firstReport;
    var sampleDataForKey, newRow;

    graph.dataPointsForScaling = [];
    service.data = keys.map(function(key){
      var newRow = {};
      sampleDataForKey = firstReport[key]
      newRow.day = sampleDataForKey.day;
      newRow.week = sampleDataForKey.week;
      newRow.month = sampleDataForKey.month;
      newRow.year = sampleDataForKey.year;
      newRow.date = sampleDataForKey.date;

      valueObject = calculateRowValue.call(service, key);
      newRow.values = valueObject.value;
      newRow.variables = valueObject.variables;
      return newRow;
    });
  };

  var calculateRowValue = function(rowKey){
    var service = this, keys = service.keys, 
        graph = service.graph, 
        reportType = graph.reportType,
        firstReport = service.firstReport,
        valueObject, hasGrouping, operatorName,
        operatorParams;
    hasGrouping = Graph.utils.exists(graph.groupBy);

    valueObject = {};
    valueObject.variables = {};
    operatorName = Graph.utils.dashedNameToCamelized(reportType.type);
    operatorParams = [];

    //get variable values
    reportType.values.forEach(function(valueType){
      var val = rowVal.call(service, rowKey, valueType);
      valueObject.variables[valueType] = val;
      operatorParams.push(val);
    });

    if(hasGrouping){
      var valueSumForScaling = 0;
      valueObject.value = graph.allGroupByValues.reduce(function(values, groupByKey){
        var paramsForGroupKey = [];
        operatorParams.forEach(function(operatorParam){
          paramsForGroupKey.push(operatorParam[groupByKey]);
        });
        values[groupByKey] = Graph.data.operators.callOperator(operatorName, paramsForGroupKey);

        if(graph.cumulativeScaling){
          valueSumForScaling += values[groupByKey];
        }else{
          graph.dataPointsForScaling.push(values[groupByKey]);
        }

        return values;
      },{});
      if(graph.cumulativeScaling) graph.dataPointsForScaling.push(valueSumForScaling);

    }else{
      valueObject.value = Graph.data.operators.callOperator(operatorName, operatorParams);
      graph.dataPointsForScaling.push(valueObject.value);
    }
    return valueObject;
  };

  var rowVal = function(rowKey, valType){
    var service = this,
        graph = service.graph, 
        allGroupByValues = graph.allGroupByValues,
        data = service.data,
        firstReport = service.firstReport,
        hasGrouping, report, reportKeyAndVal;
    hasGrouping = Graph.utils.exists(graph.groupBy);

    reportKeyAndVal = Graph.utils.getReportKeyAndValueType(valType);
    if(Graph.utils.exists(reportKeyAndVal.reportKey)){
      report = data[reportKeyAndVal.reportKey];
    }else{
      report = firstReport;
    }

    if(hasGrouping){
      return allGroupByValues.reduce(function(vals, groupByKey){
        vals[groupByKey] = parseFloat(report[rowKey][groupByKey][reportKeyAndVal.valueType] || 0);
        return vals;
      }, {});
    }else{
      return parseFloat(report[rowKey][reportKeyAndVal.valueType] || 0);
    }
  };

  return function(){
    var graph = this;
    var serviceInstance = {};
    serviceInstance.graph = graph;
    serviceInstance.data = graph.mungedData;
    serviceInstance.firstReport = Graph.utils.getFirstFromObject(serviceInstance.data);
    serviceInstance.keys = d3.map(serviceInstance.firstReport).keys();
    serviceInstance.hasGrouping = Graph.utils.exists(graph.groupBy);
    addRowValues.call(serviceInstance);
    return serviceInstance.data;
  };
}();

//Munge soes not get the asked for values, it only transforms the
//data into a map of date to rows, where the rows can also be nested by the 
//group by field

Graph.functions.munge = function(){

  //SERVICE INSTANCE METHODS
  var retrieveGroupByValues = function(){
    var service = this;
    var groupByField = service.graph.groupBy,
        data = service.data;
    if(!Graph.utils.exists(groupByField)) return [];
    return data.reduce(function(keystore, row){
      keystore.add(row[groupByField]);
      return keystore;
    }, d3.set()).values();
  };

  var addAggregateKeys = function(){
    var service = this,
        dateRangeAggregateType = service.dateRangeAggregateType, 
        groupByField = service.graph.groupBy;
    service.data = service.data.map(function(row){
      row.aggregateKey = aggregateKey(row, dateRangeAggregateType,
        groupByField);
      return row;
    });
  };

  var rollUp = function(){
    var service = this, data = service.data;
    var nested = d3.nest()
      .key(function(row){return row.aggregateKey;})
      .sortKeys(d3.ascending)
      .entries(data);
    service.data = nested.reduce(function(map, keyValuePair){
      map[keyValuePair.key] = keyValuePair.values;
      return map;
    }, {});
  };

  //Also removes keys that shouldn't be there
  var fillInMissingKeys = function(){
    var service = this, data = service.data,
        dateKeys = service.dateKeys, 
        allGroupByValues = service.allGroupByValues,
        dataForKey;
    var cleanedKeys = {};
    dateKeys.forEach(function(dateKey){
      if(service.hasGrouping){
        allGroupByValues.forEach(function(groupByValue){
          dataForKey = data[dateKey+":"+groupByValue] || [];
          cleanedKeys[dateKey+":"+groupByValue] = dataForKey;
        });
      }else{
        dataForKey = data[dateKey] || [];
        cleanedKeys[dateKey] = dataForKey;
      }
    });
    service.data = cleanedKeys;
  };

  var sumVariablesForRows = function(){
    var service = this, graph = service.graph,
        variableFields = service.variableFields;
    var dataKeys = d3.map(service.data).keys();
    var rows, dateParts;
    var sum = function(rows, valueKey){
      return rows.reduce(function(sum, row){
        if(isRowValid(row, service.whereExpressions)){
          return sum + (parseFloat(row[valueKey]) || 0);
        }else{
          return sum;
        }
      }, 0);
    };
    service.data = dataKeys.reduce(function(summed, dataKey){
      rows = service.data[dataKey];
      summed[dataKey] = variableFields.reduce(function(summedRow, variableField){
        summedRow[variableField] = sum(rows, variableField);
        return summedRow;
      }, {});
      return summed;
    }, {});
  };

  var collapseGrouping = function(){
    var service = this, data = service.data,
        dateKeys = service.dateKeys, 
        allGroupByValues = service.allGroupByValues,
        dataForKey;
    service.data = dateKeys.reduce(function(collapsed, dateKey){
      collapsed[dateKey] = {};
      allGroupByValues.forEach(function(groupByValue){
        dataForKey = data[dateKey+":"+groupByValue];
        collapsed[dateKey][groupByValue] = dataForKey;
      });
      return collapsed;
    }, {});
  };

  var addDateMetaData = function(){
    var service = this;
    var dateParts;
    service.dateKeys.forEach(function(dateKey){
      service.data[dateKey].date = service.dateKeysToDate[dateKey];
      dateParts = Graph.utils.getDateParts(service.data[dateKey].date);
      service.data[dateKey].day = dateParts.day;
      service.data[dateKey].week = dateParts.week;
      service.data[dateKey].month = dateParts.month;
      service.data[dateKey].year = dateParts.year;
    });
  };

  //PRIVATE METHODS 
  var isRowValid = function(row, where){
    var valid = true;
    var whereMap = d3.map(where);
    whereMap.forEach(function(key, val){
      if(valid) valid = row[key] == val;
    });
    return valid;
  };

  var aggregateKey = function(row, dateRangeAggregateType, groupByField){
    var rowDate = Graph.utils.getDateFromReportRow(row);
    var aggregateKey = aggregateKeyForDate(rowDate, dateRangeAggregateType);
    if(groupByField){
      aggregateKey += ":"+row[groupByField];
    }
    return aggregateKey;
  };

  var aggregateKeyForDate = function(date, dateRangeAggregateType){
    var dateParts = Graph.utils.getDateParts(date);
    var day = dateParts.day, week = dateParts.week, 
        month = dateParts.month, year = dateParts.year;
    var aggregateKey;
    switch(dateRangeAggregateType){
    case Graph.dateAggregate.month:
      aggregateKey = year+"-"+month;
      break;
    case Graph.dateAggregate.week:
      aggregateKey = year+"-"+week;
      break;
    default:
      aggregateKey = year+"-"+month+"-"+day;
    }
    return aggregateKey;
  };

  return function(){
    var graph = this;
    var reports = graph.rawData,
        reportKeys = d3.map(reports).keys(),
        reportData;
    var serviceInstance, variableFields, reportNameRegex, whereExpressions;

    return reportKeys.reduce(function(mungedReports, reportKey){
      reportData = reports[reportKey];
      serviceInstance = {};
      serviceInstance.graph = graph;
      serviceInstance.data = reportData;
      serviceInstance.dateRange = graph.dateRange();
      serviceInstance.dateRangeAggregateType = graph.aggregate;
      serviceInstance.allGroupByValues = retrieveGroupByValues.call(serviceInstance);
      graph.allGroupByValues = serviceInstance.allGroupByValues;
      serviceInstance.hasGrouping = Graph.utils.exists(graph.groupBy);
      serviceInstance.dateKeysToDate = {};
      serviceInstance.dateKeys = d3.set(serviceInstance.dateRange.map(function(date){
        var aggKey = aggregateKeyForDate(date, graph.aggregate);
        serviceInstance.dateKeysToDate[aggKey] = date;
        return aggKey;
      })).values();
      console.log(serviceInstance.dateKeys);

      //Where and group by can specify which report they apply to
      reportNameRegex = new RegExp('^'+reportKey+'#');
      if(reportKeys.length == 1){
        variableFields = graph.reportType.values.map(function(valueKey){
          return valueKey.replace(reportNameRegex, '');
        });

        whereExpressions = d3.map(graph.where).keys().reduce(function(expressions, whereKey){
          expressions[whereKey.replace(reportNameRegex, '')] = graph.where[whereKey];
          return expressions;
        }, {});
      }else{
        variableFields = graph.reportType.values.reduce(function(variableFields, valueKey){
          if(valueKey.search(reportNameRegex) > -1){
            variableFields.push(valueKey.replace(reportNameRegex, ''));
          }
          return variableFields;
        }, []);

        whereExpressions = d3.map(graph.where).keys().reduce(function(expressions, whereKey){
          if(whereKey.search(reportNameRegex) > -1){
            expressions[whereKey.replace(reportNameRegex, '')] = graph.where[whereKey];
          }
          return expressions;
        }, {});
      }

      serviceInstance.variableFields = variableFields;
      serviceInstance.whereExpressions = whereExpressions;


      addAggregateKeys.call(serviceInstance);
      rollUp.call(serviceInstance);
      fillInMissingKeys.call(serviceInstance);
      sumVariablesForRows.call(serviceInstance);
      if(serviceInstance.hasGrouping){
        collapseGrouping.call(serviceInstance);
      }
      addDateMetaData.call(serviceInstance);
      mungedReports[reportKey] = serviceInstance.data;
      return mungedReports;
    },{});

  };
}();


Graph.functions.dateRange = function(opts){
  var range;
  var graph = this;
  var startDate = graph.startDate,
      endDate = graph.endDate;

  if(!opts) var opts = {increment: 0, step: 1}
  var step = opts.step || 1;
  var increment = opts.increment || 0;
  switch(graph.aggregate){
  case Graph.dateAggregate.month:
    //make startDate start of month
    startDate = d3.time.month.floor(startDate);
    endDate = d3.time.month.ceil(d3.time.month.offset(endDate, increment));
    range = d3.time.month.range(startDate, endDate, step);
    break;
  case Graph.dateAggregate.week:
    //dates in report data have monday as week start
    startDate = d3.time.monday.floor(startDate);
    endDate = d3.time.monday(d3.time.monday.offset(endDate, increment));
    range = d3.time.monday.range(startDate, endDate, step);
    break;
  default:
    if (startDate.getTime() == endDate.getTime()) {
      return [startDate];
    };
    range = d3.time.day.range(startDate, d3.time.day.offset(endDate, 1), step);
  }
  return range;
};

