;var Graph = Graph || {};
Graph.functions = Graph.functions || {};
var GraphNew = (function(){
  var dateFormat = d3.time.format("%Y-%m-%d");
  var create = function(containerReference, width, height){
    var allDataPoints, allDates, yAxis, xAxis;
    //Graph Instance Variables
    var graphInstance = {};
    var totalWidth = width;
    var totalHeight = height;
    var _container = d3.select(containerReference);
    _container.selectAll('svg').data([1]).enter()
            .append('svg')
            .attr({
              'class' : "svg-graph",
              'height': height,
              'width': width
            });
    graphInstance._svg = _container.select('svg');
    graphInstance.height = height;
    graphInstance.width = width;
    var responsiveProportion = graphInstance.width / 1000;
    if (responsiveProportion > 1.0) {
      responsiveProportion = 1;
    };
    graphInstance.rightSideSpacing = 100 * responsiveProportion;
    var totalHeight = graphInstance.height,
        totalWidth = graphInstance.width;
    graphInstance.graphHeight = totalHeight - Graph.spacing.svgMargin;
    graphInstance.graphWidth = totalWidth - (2*Graph.spacing.svgMargin) - graphInstance.rightSideSpacing;

    //Append instance functions to graph
    for(k in Graph.functions){
      if(typeof(Graph.functions[k]) == "function"){
        graphInstance[k] = Graph.functions[k];
      }
    }

    graphInstance.parseOpts = function(opts){
      var setValueFromOpts = function(key, dflt){
        if(Graph.utils.exists(opts[key])){
          graphInstance[key] = opts[key];
        }else if(!Graph.utils.exists(graphInstance[key])){
          graphInstance[key] = dflt;
        }
      };
      setValueFromOpts('cumulativeScaling', false);
      setValueFromOpts('where', null);
      setValueFromOpts('groupBy', null);
      setValueFromOpts('title', 'Graph Title');
      setValueFromOpts('units', '');
      setValueFromOpts('prefixUnits', false);
      setValueFromOpts('aggregate', Graph.dateAggregate.day);
      setValueFromOpts('startDate', new Date());
      setValueFromOpts('endDate', new Date());
      setValueFromOpts('drawFunction', Graph.graphingFunctions.drawNothing);
    };

    graphInstance.setData = function(data, reportType, opts){
      this.rawData = data;
      this.reportType = reportType;
      if(!opts) var opts = {}
      this.reparseData(opts);
    };

    graphInstance.reparseData = function(opts){
      if(!opts) var opts = {}
      this.parseOpts(opts);
      this.mungedData = graphInstance.munge();
      this.parsedData = graphInstance.parseData();
    };

    return graphInstance;
  };

  return {
    dateFormat : dateFormat,
    create : create
  };
})();
$.extend(Graph, GraphNew);

