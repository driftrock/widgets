;var Graph = Graph || {};

Graph.utils = {
  exists : function(val){
    return (typeof(val) != "undefined" && val != null);
  },

  movingAverage : function(data, windowSize, func){
    var currentWindow = [];
    return data.map(function(dataElement){
      var value, sum, movingAverage;
      value = func(dataElement);
      currentWindow.push(value);
      movingAverage = null;
      if(currentWindow.length == windowSize){
        sum = currentWindow.reduce(function(memo, val){
          return memo += val;
        }, 0);
        movingAverage = sum / windowSize;
        currentWindow.shift();
      }
      dataElement['movingAverage'] = movingAverage;
      return dataElement;
    });
  },

  getFirstFromObject : function(object){
    return object[d3.map(object).keys()[0]];
  },

  getReportKeyAndValueType : function(valueType){
    var reportKey = null;
    var match = valueType.match(/(.+)#(.+)/);
    if(match && match.length > 0){
      reportKey = match[1];
      valueType = match[2];
    }
    return {reportKey: reportKey, valueType: valueType}
  },

  dashedNameToCamelized : function(string){
    return string.replace(/\-(.)/g, function(match, p1){
      return p1.toUpperCase();
    });
  },

  getDateFromReportRow : function(row){
    //javascript week and month is zero based
    return new Date(row.year, row.month - 1, row.day);
  },

  getDateParts : function(date){
    return {
      day   : date.getDate(),
      week  : d3.time.mondayOfYear(date),
      month : date.getMonth(), 
      year  : date.getFullYear()
    }
  },

  getTranslation : function(selection){
    var currentTransform = selection.attr('transform');
    var translateRegex = /translate\(([0-9.]+)\s*,\s*([0-9.]+).*\).*/;
    var translate = currentTransform.match(translateRegex);
    return [parseFloat(translate[1]), parseFloat(translate[2])];
  },

  moveTicks : function(graph, xMove, yMove){
    var translation = this.getTranslation(graph.xTicks);
    var xTranslate = translation[0];
    var yTranslate = translation[1];
    var newXTranslate = xTranslate + xMove;
    var newYTranslate = yTranslate + yMove;
    graph.xTicks.attr('transform',
        'translate('+newXTranslate+', '+newYTranslate+')')
  },

  humanize : function(strOrNum, decimalPlaces){
    if(typeof(strOrNum) == "string" ){
      return strOrNum.replace(/_/g, ' ')
        .replace(/(\w+)/g, function(match) {
          return match.charAt(0).toUpperCase() + match.slice(1);
        });
    }else{
      if(typeof decimalPlaces === 'undefined') { var decimalPlaces = 2; }
      return $.number(strOrNum, decimalPlaces);
    }
  },

  toDateStr : function(date) {
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    return date.getDate() + " " + months[date.getMonth()];
  },

  attachTooltip : function(selector){
    var containerId = jQuery(selector).parents('.graph-container').attr('id');
    jQuery(selector).each(function(){
      jQuery(this).tooltip({
        'container' : 'section#'+containerId,
        'toggle' : 'hover'
      });
    });
  },

  driftrockColourScale : function(domain){
    var colourScale = d3.scale.category20c().domain(domain);
    //Init colour scale so it is always the same
    for(var i=domain[0]; i < domain[1]; i++) colourScale(i);
    return colourScale;
  },

  numberForString : function(str){
    var alphabet = 'abcdefghijklmnopqrstuvwxyz'.split(''),
        strNum = '',
        maxLength = Math.min(str.length, 3);
    for(var c=0; c < maxLength; c++){
      var alphaIndex = alphabet.indexOf(str[c].toLowerCase());
      if(alphaIndex >= 0){
        strNum += ""+alphaIndex;
      }
    }
    return parseInt(strNum, 10) * str.length;
  },

  getColourForStackKey : function(stackKey){
    var stackKeyNum, colourNum, colour,
      maxNum = parseInt((new Array(10)).join('26'), 10),
      numberOfColours = 20,
      colourScale = Graph.utils.driftrockColourScale([0, 20]);
    if (stackKey.match(/google.*cpc/i) || stackKey.match(/^adwords$/i)){
      return "#009900";
    }else if (stackKey.match(/facebook.*(cpc|paid)/i) ||
        stackKey.match(/^facebook$/i)){
      return "#4a66a0";
    }else if(stackKey.match(/(twitter|t.co).*(cpc|paid)/i)){
      return "#3abaec";
    }else if(stackKey.match(/other/i)){
      return "#e0e0e0";
    }else{
      stackKeyNum = Graph.utils.numberForString(stackKey);
      colourNum = Math.ceil((stackKeyNum / (5000 + Math.abs(stackKeyNum))) * numberOfColours);
      colour = colourScale(colourNum);
      return colour;
    }
  },

  tooltipTitle : function(date, value, graph, label){
    var strDate = this.toDateStr(date);
    var precision = 0
    if (value > 0 && value < 1)
      precision = 2;
    var val = this.humanize(value, precision);
    var valAndUnits = val + " " + graph.units;
    if(graph.prefixUnits){
      valAndUnits = this.unitsToSymbol(graph.units) + val;
    }
    if(label){
      return this.humanize(label) + " : "+strDate+" : "+valAndUnits;
    }else{
      return strDate+" : "+valAndUnits;
    }
  },

  unitsToSymbol : function(units){
    if(units == "&#36;"){
      return "$";
    }else if(units == "&pound;"){
      return "£";
    }else if(units == "&euro;"){
      return "€";
    }else{
      return units;
    }
  }
};
