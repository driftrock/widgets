;var Graph = Graph || {};

Graph.spacing = {
  padding : 50,
  svgMargin : 50,
  labelWidth : 80
}

Graph.dateAggregate = {
  day : 0,
  week : 1,
  month : 2
};
