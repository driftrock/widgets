;var Graph = Graph || {};
Graph.functions = Graph.functions || {};

Graph.functions.draw = function(){
  this.redraw();
};

Graph.functions.redraw = (function(){
  var resetSVG = function(graph){
    graph._svg.selectAll("*").remove()
    graph._svg.attr({
            'height': graph.height,
            'width': graph.width
            });
  };

  var prepareScales = function(graphInstance){
    var allDataPoints = graphInstance.dataPointsForScaling;
    var increment = (allDataPoints.length < 4 ? 1 : 0);
    graphInstance.xScale = d3.time.scale()
      .domain(d3.extent(graphInstance.dateRange({
        increment: increment
        })))
      .range([0, graphInstance.graphWidth]);

    graphInstance.yScale = d3.scale.linear()
      .domain([0, d3.max(allDataPoints)])
      .rangeRound([graphInstance.graphHeight - 5, 0]);
  };

  var drawAxes = function(graphInstance){
    var minDataPoint, maxDataPoint,
      graphWidth, graphHeight, 
      tickInterval, ticksPerLabel,tickFormat,
      xScale, yScale;
    var allDataPoints = graphInstance.dataPointsForScaling,
        _svg = graphInstance._svg;
    minDataPoint = d3.min(allDataPoints);
    maxDataPoint = d3.max(allDataPoints);
    graphWidth = graphInstance.graphWidth;
    graphHeight = graphInstance.graphHeight;
    ticksPerLabel = Math.ceil(allDataPoints.length / 
        (graphWidth / Graph.spacing.labelWidth));
    xScale = graphInstance.xScale;
    yScale = graphInstance.yScale;
    switch(graphInstance.aggregate){
      case Graph.dateAggregate.month:
        tickInterval = d3.time.month;
        tickFormat = "%Y %b";
        break;
      case Graph.dateAggregate.week:
        tickInterval = d3.time.monday;
        tickFormat = "%Y wk: %W";
        break;
      default:
        tickInterval = d3.time.day;
        tickFormat = "%a %d/%m";
    }
    var startDate = graphInstance.startDate,
        endDate = graphInstance.endDate;
    var ticks = tickInterval.range(startDate, endDate, ticksPerLabel).length
    xAxis = d3.svg.axis()
      .scale(xScale)
      .orient("bottom")
      .ticks(ticks)
      .tickFormat(d3.time.format(tickFormat));
    
    graphInstance.xTicks = _svg.append('g')
      .attr({
        'transform': 'translate('+ Graph.spacing.svgMargin +', ' + (graphHeight) + ')',
        'id': 'x_axis',
        'class': 'axis'
      }).call(xAxis);

    yAxis = d3.svg.axis()
      .scale(yScale)
      .orient("left");

    graphInstance.yTicks = _svg.append('g')
      .attr({
        'transform': 'translate(' + (Graph.spacing.svgMargin)+ ', 0)',
        'id': 'y_axis',
        'class': 'axis'
      }).call(yAxis);
  };

  var drawGrid = function(graphInstance){
    graphInstance._svg.append('g')
      .attr({
        'class': 'grid',
        'transform': 'translate(' + Graph.spacing.svgMargin + ', 0)'
      })
      .call(yAxis
        .tickSize(-graphInstance.graphWidth-graphInstance.rightSideSpacing, 0, 0)
        .tickFormat("")
      );
  };

  return function(opts){
    if(!Graph.utils.exists(this.rawData)) return;
    if(opts){
      this.reparseData(opts);
    }
    resetSVG(this);
    prepareScales(this);
    drawAxes(this);
    drawGrid(this);
    this._g = this._svg.append('g').
      attr({
        'transform' : 'translate('+Graph.spacing.svgMargin+', '+ 0 +')'
      });
    this.drawFunction.call(this);
  };
})();
