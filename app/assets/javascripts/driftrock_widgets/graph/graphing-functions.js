;
var Graph = Graph || {};

Graph.graphingFunctions = {
  drawNothing : function(){},

  barChartWithMa : function(){
    var movingAverageLine;
    var graph = this, movingAverageWindow = 3,
      x = graph.xScale, y = graph.yScale,
      drawHeight = graph.graphHeight;


    if (graph.parsedData.length == 0) {
      return;
    };

    var barWidth = graph.graphWidth/graph.parsedData.length;

    Graph.utils.moveTicks(graph, barWidth/2, 0);
    var barData = graph.parsedData.reduce(function(memo, row){
      if(row.values != null && !isNaN(row.values)){
        memo.push(row);
      }
      return memo;
    }, []);
    var bars = graph._g.selectAll("rect.bar").data(barData);
    bars.enter().append("rect")
        .classed("bar", true)
        .attr("x", function(d, i){return x(d.date);})
        .attr("y", function(d, i){return drawHeight;})
        .attr("width", barWidth)
        .attr("height", function(){return 0});

    bars.transition()
        .duration(1000)
        .attr("height", function(d, i){
          return drawHeight - y(d.values);
        })
        .attr("y", function(d, i){
          return y(d.values);
        })
        .each("end", function(){
          d3.select(this).attr("title", function(d, i){
            return Graph.utils.tooltipTitle(d.date, d.values, graph);
          });
          Graph.utils.attachTooltip(this);
        });

     var withMovingAverage = Graph.utils.movingAverage(
         graph.parsedData, movingAverageWindow, function(d){
           return d.values;
         });

    var startOfLine = d3.svg.line()
      .x(function(d){return x(d.date);})
      .y(function(){return y(0)})
      .interpolate('basis')
      .defined(function(datum){ return (datum.movingAverage != null && !isNaN(datum.movingAverage));});

    movingAverageLine = d3.svg.line()
      .x(function (datum) { return x(datum.date) + (barWidth/2); })
      .y(function (datum) { return y(datum.movingAverage); })
      .interpolate('basis')
      .defined(function(datum){ return (datum.movingAverage != null && !isNaN(datum.movingAverage));});

    var movingAverage = graph._g.selectAll('path.moving-average')
      .data([1]).enter()
      .append('path');
    movingAverage.attr({
        'd': startOfLine(withMovingAverage),
        'class': 'moving-average'
      }).transition()
        .duration(1000)
        .attr('d', movingAverageLine(withMovingAverage));

    //Legend
    var xOffset = 50;
    var xTicksTranslation = Graph.utils.getTranslation(graph.xTicks);
    var legendY = xTicksTranslation[1] + 40;
    graph._svg.append("path")
      .classed("legend-moving-average", true)
      .attr({
      'd' : (d3.svg.line().x(function(d) { return d;}).y(legendY - 5)
        .interpolate('linear'))([xOffset, xOffset + 50])
      });
    graph._svg.append("text").classed("legend", true)
      .attr({
        x: xOffset + 60,
        y: legendY
      })
      .text("Moving Average")
  },


  stackedBarChart : function(){
    var numberOfStackedBarsToShow = 5;
    var movingAverageLine;
    var graph = this,
      x = graph.xScale, y = graph.yScale,
      drawHeight = graph.graphHeight;

    if (graph.parsedData.length == 0) return;

    var barWidth = (graph.graphWidth/graph.parsedData.length);
    Graph.utils.moveTicks(graph, barWidth/2, 0);
    var stackKeys = d3.set(d3.map(graph.parsedData[0].values).keys());
    stackKeys = stackKeys.values();
    
    var limitStackBarChartData = function(data, stackKeys, limit){
      return data.map(function(datum){
        //Clone datum
        datum = $.extend({}, datum);
        var sortedKeys, keysToSum, keysToUse, newValues, otherSum;
        sortedKeys = stackKeys.sort(function(key1, key2){
          var key2Val =datum.values[key2],
              key1Val =datum.values[key1];
          if(key2Val && key1Val) return key2Val - key1Val;
          if(key2Val) return 1;
          if(key1Val) return -1;
          return 0;
        });
        keysToUse = sortedKeys.slice(0, limit);
        keysToSum = sortedKeys.slice(limit-1);
        newValues = {};
        otherSum = keysToSum.reduce(function(sum, key){
          var val = datum.values[key];
          return (val ? val + sum : sum);
        }, 0);
        keysToUse.forEach(function(key){
          newValues[key] = datum.values[key];
        });
        newValues['other'] = otherSum;
        
        datum.values = newValues;
        return datum;
      });
    };
    
    var limitedData = limitStackBarChartData(graph.parsedData,
        stackKeys, numberOfStackedBarsToShow);
    stackKeys.push('other');


    var currentStackValue = {};
    var getStackValue = function(datum, stackKey){
      var value = datum.values[stackKey] || 0;
      if(currentStackValue[datum.date]){
        value += currentStackValue[datum.date];
      }
      currentStackValue[datum.date] = value;
      return y(value);
    };

    stackKeys.forEach(function(stackKey, index){
      var stackKeyClass = stackKey.replace(/^([0-9])/, "N$1").replace(/\./g, "\-").replace(/[^a-zA-Z0-9\-]/g, "");
      var bars = graph._g.selectAll("rect.stackedbar."+stackKeyClass)
                .data(limitedData);
      bars.enter().append("rect")
          .classed(stackKeyClass, true)
          .classed("stackedbar", true)
          .attr("x", function(datum, index){
            return x(datum.date);
          })
          .attr("y", function(datum, index){
            return getStackValue(datum, stackKey);})
          .attr("width", barWidth)
          .attr("height", function(datum, index){
            if(!datum.values[stackKey]) return 0;
            return drawHeight - y(datum.values[stackKey]);
          }).style('fill', function(){
            return Graph.utils.getColourForStackKey(stackKey)})
          .attr("title", function(datum, index){
            return Graph.utils.tooltipTitle(datum.date, datum.values[stackKey], graph, stackKey);
          });
      Graph.utils.attachTooltip('rect.'+stackKeyClass);
    });

  }
};
