var Widgets = (function ($) {
  var startDate, endDate;
  var currentGraphs = {};

  var getWidth = function($this) {
    return $this.width()-20;
  };

  var deselectWidgets = function(graphElementId){
    var selector= ".widget[data-graph-element-id="+graphElementId+"]";
    $(selector).each(function(_, el){
      $(el).removeClass("current");
    });
  };

  var selectWidget = function(widget){
    var graphElementId = widget.data('graph-element-id');
    deselectWidgets(graphElementId);
    widget.addClass("current");
  };

  var aggregate = function(graphElementId){
    var containerSelector = "#graph_container_"+graphElementId;
    var aggregateStr = $('.active[data-aggregate]', containerSelector).
      data('aggregate');
    return Graph.dateAggregate[aggregateStr];
  };

  var showGraph = function(event) {
    var graph, graphElementSelector;
    $('#stat-click-note').hide();
    selectWidget($(this));
    graph = event.data.graph;
    graph.reparseData({aggregate: aggregate(event.data.graphElementId)});
    graph.draw();
    currentGraphs[event.data.graphElementId] = graph;
    graphElementSelector = "#graph_container_"+event.data.graphElementId;
    $('.graph-title', graphElementSelector).text(graph.title);

    $(graphElementSelector).
      addClass('open').
      removeClass('closed').
      removeClass('start-closed');
  };

  var init = function(startDateEl, endDateEl){
    initDates(startDateEl, endDateEl);
    initWidgets();
    initRedraw();
    initHide();
  };

  var initDates = function(startDateEl, endDateEl){
    var getDateFromField = function(field){
      return d3.time.format("%d %b %Y").parse($(field).val());
    };
    startDate = getDateFromField(startDateEl);
    endDate = getDateFromField(endDateEl);
  };

  var initHide = function(){
    $('[data-hide-graph]').bind('click', function(evt){
      var container = $(this).parents('.graph-container');
      container.removeClass('open').addClass('closed');
    });
  };

  var initRedraw = function(){
    $('[data-redraw-graph]').bind('click', function(){
      var graphContainerId, graphElementId, graphToRedraw, agg, _me = $(this);
      graphContainerId = _me.parents('.graph-container').attr('id');
      graphElementId = graphContainerId.replace(/^graph_container_/, '');

      if(_me.data('aggregate')){
        agg = Graph.dateAggregate[_me.data('aggregate')];
      }else{
        agg = aggregate(graphElementId);
      }
      graphToRedraw = currentGraphs[graphElementId];
      if(graphToRedraw) {
        graphToRedraw.redraw({aggregate: agg});
      }
    });
  };

  var newGraph = function(graphElementId) {
    var graphSelector;
    var graphWidth = $('.main').width();
    if($(window).width() >=768) {
      graphWidth = $('.main').width()-90;
    }
    graphSelector = "#graph_"+graphElementId;
    return Graph.create(graphSelector, graphWidth, 300);
  };

  var humanizeWidgetDisplayValue = function(displayValue,
                            decimalPlaces, units, prefixUnits){
    if (displayValue == 0) return '-';
    var human = Graph.utils.humanize(displayValue, decimalPlaces);
    human = prefixUnits ? units + human : human + " "+ units;
    return human;
  };

  var initWidgets = function() {
    $(".widget[data-report-type]").each(function(_, widget) {
      var graph, displayValue, height, reportData;
      _widget = $(widget);
      if(_widget.hasClass('coming-soon')) return;
      var reportDataKeys   = _widget.data("reports") || [_widget.data("report")],
        reportType     = _widget.data("report-type"),
        graphElementId = _widget.data('graph-element-id'),
        opts           = _widget.data();

      reportData = reportDataKeys.reduce(function(reportData, reportDataKey){
        reportData[reportDataKey] = $('.reports-data-holder').data(reportDataKey);
        return reportData;
      }, {});

      opts.aggregate  = aggregate(graphElementId);
      opts.startDate  = startDate;
      opts.endDate    = endDate;
      if(_widget.data("group-by")){
        opts.drawFunction = Graph.graphingFunctions.stackedBarChart;
        opts.cumulativeScaling = true;
      }else{
        opts.drawFunction = Graph.graphingFunctions.barChartWithMa;
      }

      graph = newGraph(graphElementId);
      graph.setData(reportData, reportType, opts);
      displayValue = graph.total();
      $('.content', _widget).text(
        humanizeWidgetDisplayValue(displayValue,
          _widget.data('decimal-places'),
          _widget.data('units'),
          _widget.data('prefix-units')));
      height = 50;
      _widget.bind("click", {graph: graph,
        graphElementId: graphElementId}, showGraph);
    });

    $(".widget[data-link]").bind("click", function(evt){
      var _widget = $(this),
          linkLocation = _widget.data("link");
      window.location = linkLocation;
    });
  }

  return {
    init: init
  }

}(jQuery));
