describe("Graph.data", function(){

  describe("#munge", function(){
    var graph;

    beforeEach(function(){
      graph = Graph.create('#blah', 100, 100);
    });

    it("should munge the data such that it is split by supplied where fields", function(){
      var data, reportType, opts, parsedData,
        expectedRow, expectedConversions;
      data = {conversions: Factories.graphData.conversions()};
      reportType = {type: 'identity', values: ['sum_conversions']};
      opts = {
        'where' : {'goal_name' : 'Purchases'},
        'startDate' : new Date(2013, 9, 28),
        'endDate' : new Date(2013, 9, 29)
      }
      expectedRow = data.conversions.reduce(function(foundRow, row){
        if(row.day == 28 && row.month == 10 && row.year == 2013
          && row.goal_name == "Purchases"){
          foundRow = row;
        }
        return foundRow;
      }, null);
      expectedConversions = parseInt(expectedRow['sum_conversions'], 10);
      graph.setData(data, reportType, opts);
      parsedData = graph.munge();
      expect(parsedData.conversions['2013-9-28'].sum_conversions).toBe(expectedConversions);
    });

    it("should group values by a given group by field", function(){
      var data, reportType, opts, parsedData, expectedKeys,
        parsedValues, valueKeys;
      data = {conversions: Factories.graphData.conversions()};
      reportType = {type: 'identity', values: ['sum_conversions']};
      opts = {
        'groupBy' : 'goal_name',
        'startDate' : new Date(2013, 9, 28),
        'endDate' : new Date(2013, 9, 29)
      }
      graph.setData(data, reportType, opts);
      parsedData = graph.munge();
      parsedValues = parsedData.conversions['2013-9-28'];
      valueKeys = d3.map(parsedValues).keys();
      expectedKeys = ['Adds to cart', 'Purchases', 'Sign ups'];
      expectedKeys.forEach(function(key){
        expect(valueKeys).toContain(key);
      });
    });

    it("should make sure there are rows for each date in the range",
        function(){
      var data, reportType, opts, parsedData, startDate, endDate,
        expectedDates, parsedDates, parsedDataKeys;
      data = {conversions: Factories.graphData.conversions()}
      reportType = {type: 'identity', values: ['sum_conversions']};
      startDate = new Date(2013, 8, 1);
      endDate = new Date(2013, 9, 29);
      opts = {
        'startDate' : startDate,
        'endDate' : endDate
      }
      graph.setData(data, reportType, opts);
      parsedData = graph.munge();
      expectedDates = d3.time.day.range(startDate, endDate);
      parsedDataKeys = d3.map(parsedData.conversions).keys();
      expect(parsedDataKeys.length).toBe(expectedDates.length);
      parsedDates = parsedDataKeys.map(function(row){
        return parsedData.conversions[row].date;
      });
      expectedDates.forEach(function(date){
        expect(parsedDates).toContain(date);
      });
    });

  it("ensures there's at least one date if date range spans one day",
        function(){
      var data, reportType, opts, parsedData, startDate, endDate,
        expectedDates, parsedDates, parsedDataKeys;
      data = {conversions: Factories.graphData.conversions()};
      reportType = {type: 'identity', values: ['sum_conversions']};
      date = new Date(2013, 8, 1);
      opts = {
        'startDate' : date,
        'endDate' : date
      }
      graph.setData(data, reportType, opts);
      parsedData = graph.munge();
      parsedDataKeys = d3.map(parsedData.conversions).keys();

      expect(parsedDataKeys.length).toEqual(1);
      expect(parsedData.conversions[parsedDataKeys[0]].date).toEqual(date)
    });
  });
});
