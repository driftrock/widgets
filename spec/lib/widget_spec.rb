require 'spec_helper'
module DriftrockWidgets
  describe Widget do

    let(:context) { double(:context) }

    subject(:report) do
      report = double(:report)
      report.stub(:report_types_mapping)
      report
    end

    subject(:channels) { double(:channels) }

    subject(:widget) do
      request = double(:request)
      Widget.new(context, channels, request, {
        report: report,
        channel_types: [:some_channel]
      })
    end

    describe "#element_to_display" do
      context "when no channels are required" do
        it "should return has_data" do
          channels = double(:channels)
          request = double(:request)
          result = Widget.new(context, channels, request, {report: report}).
            send(:element_to_display)
          expect(result).to be :has_data
        end
      end

      context "when required channels are connected and have data" do
        context "if last_data_at is recent" do
          it "should return has_data" do
            channels.stub(:has_profile_for_type?).and_return(true)
            channels.stub(:pulled_data_for_type?).and_return(true)
            channels.stub(:pending_for_type?).and_return(true)
            channels.stub(:connected_channel_for_type?).and_return(true)
            result = widget.send(:element_to_display)
            expect(result).to be :connected_but_no_data
          end
        end

        context "if last_data-at is old enough" do
          it "should return has_data" do
            channels.stub(:has_profile_for_type?).and_return(true)
            channels.stub(:pulled_data_for_type?).and_return(true)
            channels.stub(:pending_for_type?).and_return(false)
            channels.stub(:connected_channel_for_type?).and_return(true)
            result = widget.send(:element_to_display)
            expect(result).to be :has_data
          end
        end
      end

      context "when required channels are connected but have no data" do
        it "should return connected_but_no_data" do
          channels.stub(:has_profile_for_type?).and_return(true)
          channels.stub(:pulled_data_for_type?).and_return(false)
          channels.stub(:pending_for_type?).and_return(true)
          channels.stub(:connected_channel_for_type?).and_return(true)
          result = widget.send(:element_to_display)
          expect(result).to be :connected_but_no_data
        end
      end

      context "when required channels are only partially connected" do
        it "should return connected_but_no_profile" do
          channels.stub(:has_profile_for_type?).and_return(false)
          channels.stub(:pulled_data_for_type?).and_return(false)
          channels.stub(:pending_for_type?).and_return(true)
          channels.stub(:connected_channel_for_type?).and_return(true)
          result = widget.send(:element_to_display)
          expect(result).to be :connected_but_no_profile
        end
      end

      context "when required channels are not connected" do
        it "should return not_connected" do
          channels.stub(:has_profile_for_type?).and_return(false)
          channels.stub(:pulled_data_for_type?).and_return(false)
          channels.stub(:pending_for_type?).and_return(true)
          channels.stub(:connected_channel_for_type?).and_return(false)
          result = widget.send(:element_to_display)
          expect(result).to be :not_connected
        end
      end
    end

    describe "set_report_type!" do
      context "when key is not a hash" do
        it "should set an identity type" do
          widget.send(:set_report_type!, "a_key")
          report_type = widget.instance_variable_get('@report_type')
          expect(report_type).to eq ({
            type: :identity, values: ["a_key"]
          })
        end
      end

      context "when value is a hash" do
        it "should set the type to be the value" do
          expected = HashWithIndifferentAccess.new({type: :some_op, values: [:some_field]})
          widget.send(:set_report_type!, expected)
          report_type = widget.instance_variable_get('@report_type')
          expect(report_type).to eq expected
        end
      end
    end

  end
end
