module DriftrockWidgets
  require 'driftrock_widgets/widget'

  class Engine < Rails::Engine
    ActiveSupport.on_load :action_controller do
      helper DriftrockWidgets::Helpers
    end
  end
end
