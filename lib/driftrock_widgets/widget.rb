module DriftrockWidgets
  class Widget
    delegate :content_tag, to: :@context
    delegate :channels_url, to: :@context
    attr_reader :widget_index

    def initialize(context, channels, request, opts = {})
      @context = context
      @request  = request
      @channels = channels
      parse_opts!(opts)
      set_units!
      @widget_index = self.class.widget_count
      self.class.increment_widget_count
    end

    def widget_element
      case element_to_display
      when :empty
        empty_element
      when :coming_soon
        coming_soon_element
      when :has_data
        data_element
      when :connected_but_no_data
        connected_no_data_element
      when :connected_but_no_profile
        connected_no_profile_element
      else
        not_connected_element
      end
    end

    private

    def element_to_display
      return :coming_soon if coming_soon?
      return :empty if empty?
      return :has_data if @channel_types == [:none]
      pulled_data = false
      has_profile = false
      connected = false
      pending = false
      @channel_types.each do |channel_type|
        has_profile = has_profile || @channels.has_profile_for_type?(channel_type)
        pulled_data = pulled_data || @channels.pulled_data_for_type?(channel_type)
        connected = connected || @channels.connected_channel_for_type?(channel_type)
        pending = pending || @channels.pending_for_type?(channel_type)
      end
      if pulled_data && !pending
        :has_data
      elsif has_profile
        :connected_but_no_data
      elsif connected
        :connected_but_no_profile
      else
        :not_connected
      end
    end

    def default_opts
      {
        title: "Widget Title",
        decimal_places: 0,
        where: nil,
        group_by: nil,
        prefix_units: false,
        value_type: :integer,
        channel_type: [:none],
      }
    end

    def parse_opts!(opts)
      opts = default_opts.merge(opts)
      @title       = opts[:title]
      @report_data_keys = Array(opts[:report_data_keys] || opts[:report_data_key])
      @units       = opts[:units]
      @where       = opts[:where]
      @group_by    = opts[:group_by]
      @decimal_places = opts[:decimal_places]
      @value_type  = opts[:value_type].to_sym
      @channel_types = Array(opts[:channel_types] || opts[:channel_type]).map(&:to_sym)
      @prefix_units= !!(opts[:prefix_units])
      @graph_element_id = opts[:graph_element_id]
      @empty       = opts[:empty] || false
      @coming_soon = opts[:coming_soon] || false
      set_report_type!(opts[:report_type]) unless empty? || coming_soon?
    end

    def coming_soon?
      @coming_soon
    end

    def empty?
      @empty
    end

    def set_report_type!(value)
      if value.is_a?(Hash)
        @report_type = HashWithIndifferentAccess.new(value)
      else
        @report_type = {type: :identity, values: [value]}
      end
    end

    def self.widget_count
      @widget_count ||= 0
    end

    def self.increment_widget_count
      @widget_count = widget_count + 1
    end

    def u(str)
      URI::escape(str)
    end

    def empty_element
      widget_id = "widget_#{widget_index}"
      content_tag(:div, class: "widget empty", id: widget_id) do
        content_tag(:div, class: "data") do
          content_tag(:div, "-", class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def coming_soon_element
      widget_id = "widget_#{widget_index}"
      content_tag(:div, class: "widget coming-soon no-data", id: widget_id) do
        content_tag(:div, class: "data") do
          content_tag(:div, "coming soon...", class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def data_element
      element_data = {
        'reports'     => @report_data_keys.to_json,
        'report-type' => @report_type.to_json,
        'units'       => @units.html_safe,
        'title'       => @title,
        'decimal-places' => @decimal_places,
        'where'       => @where.to_json,
        'group-by'    => @group_by,
        'prefix-units'=> @prefix_units,
        'graph-element-id' => @graph_element_id
      }
      widget_id = "widget_#{widget_index}"
      content_tag(:div, class: "widget", id: widget_id, data: element_data) do
        content_tag(:div, class: "data") do
          content_tag(:div, "-", class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def connected_no_data_element
      widget_id = "widget_#{widget_index}"
      content_tag(:div, class: "widget connected-no-data no-data", id: widget_id) do
        content_tag(:div, class: "data") do
          content_tag(:div, "still getting data...", class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def connected_no_profile_element
      widget_id = "widget_#{widget_index}"
      channel_type_human = @channel_types.map{|c| c.to_s.humanize}.join(" or ")
      no_profile_text = "No profile selected for #{channel_type_human}"
      element_data ={
        link: channels_url
      }
      content_tag(:div, class: "widget connected-no-profile no-data", id: widget_id,
                  data: element_data) do
        content_tag(:div, class: "data") do
          content_tag(:div, no_profile_text, class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def not_connected_element
      widget_id = "widget_#{widget_index}"
      channel_type_human = @channel_types.map{|c| c.to_s.humanize}.join(" or ")
      not_connected_text = "Not connected #{channel_type_human}"
      element_data ={
        link: channels_url
      }
      content_tag(:div, class: "widget not-connected no-data", id: widget_id,
                  data: element_data) do
        content_tag(:div, class: "data") do
          content_tag(:div, not_connected_text, class: "content") +
            content_tag(:div, @title, class: "title")
        end
      end
    end

    def set_units!
      if @value_type == :currency
        @prefix_units = true
        @units = @channels.currency_symbol_for_type(@channel_types.first) if @units.nil?
        @decimal_places = 2
      elsif @value_type == :percentage
        @units = "&#37;"
        @decimal_places = 2
      elsif @units.nil?
        @units = ""
      end
    end
  end
end
